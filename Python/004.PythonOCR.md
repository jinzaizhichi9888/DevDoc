Python OCR : 圖片轉文字 , 使用手冊
======
`OCR x Pytesseract`

前言
------
在 Python 中， 使用 [OCR] (Optical Character Recognition , 字元辨識) 
將圖片的內容轉換成一般的文本，非常簡單。

只要將相關軟體 與 Python 套件安裝完成後，即可運行程式，

這份文件就是將之前的踩坑過程記錄下來，以供想後續想要研究的開發者可以快速上手。

<br>

範例
------
`轉換目標`
### 英文

**圖片**

![001](assets/004/001.en-us.png)

**文字**

    English
    Gamma Ray Studio
    English Text
    Text Text Text ~ !!!

<br>

### 繁體中文

**圖片**

![002](assets/004/002.zh-cht.png)

**文字**

    繁體中文
    Gamma Ray 軟體工作室
    中文 文字
    文字 文字 文字 ~ !!!

<br>

### 簡體中文

**圖片**

![003](assets/004/003.zh-chi.png)

**文字**

    简体中文
    Gamma Ray 软体工作室
    中文 文字
    文字 文字 文字 ~ !!!

<br>

安裝 Tesseract
------
### Win
+ <https://github.com/UB-Mannheim/tesseract/wiki>

**環境變數**

![win-path](assets/004/009-1.win-path.png)

### Mac

    brew install tesseract 

### Linux

    apt-get install tesseract-ocr

### 驗證

    tesseract -v

![004](assets/004/004.tesseract-v.png)

<br>

Python 環境
------
### Python 版本

**python -V**

    Python 3.8.5

### PyPI
+ Pillow
+ pytesseract

```
pip3 install Pillow
pip3 install pytesseract
```

Python 範例
------
```py
from PIL import Image
import pytesseract
img_name = './001.en-us.png'
img = Image.open(img_name)
text = pytesseract.image_to_string(img, lang='eng')
print(text)
```

+ PIL : 處理圖片 `Pillow`
+ pytesseract : OCR 模組 `Pytesseract`
+ `img_name = './001.en-us.png'` : 圖片路徑 
+ `img = Image.open(img_name)` : 載入圖片
+ `text = pytesseract.image_to_string(img, lang='eng')` : 圖片轉文字，使用英文語系

**Output**

![005](assets/004/005.demo-en-us.png)

```
English

Gamma Ray Studio
English Text

Text Text Text ~ 11!
```

+ 驚嘆號的地方被辨認為 `1` ，但基本上大部分都辯認得出來

<br>

### 中文辨識
```py
from PIL import Image
import pytesseract
img_name = './002.zh-cht.png'
img = Image.open(img_name)
text = pytesseract.image_to_string(img, lang='eng')
print(text)
```
+ `img_name = './002.zh-cht.png'` : 調整載入的圖片 `中文`

**Output**

![006](assets/004/006.demo-zh.png)
```
SRE
Gamma Ray BREA TER

FX XF

XF XF XF~
```

+ 現階段，中文的轉換會變成不認識的編碼
+ 新增語言庫，可以添加更多的語言辨識

<br>

下載語言庫
------
### GitHub - Tessdata
<https://github.com/tesseract-ocr/tessdata_best>

### 英文
`eng.traineddata`

+ <https://github.com/tesseract-ocr/tessdata_best/blob/main/eng.traineddata>

### 繁體中文
`chi_tra.traineddata`

+ <https://github.com/tesseract-ocr/tessdata_best/blob/main/chi_tra.traineddata>

### 簡體中文
`chi_sim.traineddata`

+ <https://github.com/tesseract-ocr/tessdata_best/blob/main/chi_sim.traineddata>

<br>

預設路徑
------
`以 Mac 為例 `

**Tessdata 程式路徑**

+ `/usr/local/Cellar/tesseract`

**語言包路徑**

+ `/usr/local/Cellar/tesseract/4.1.3/share/tessdata`

![007](assets/004/007.language-package.png)

+ 下載語言包後，放到程式資料夾內的 `/share/tessdata` 路徑，就可以生效

<br>

配置環境變數
------
`獨立資料夾`
![008](assets/004/008.tessdata-path.png)

### Win
`C:\DevTools\tessdata`

**環境變數**

![009-2](assets/004/009-2.win-path.png)

<br>

### Mac

`/Users/Enoxs/DevTools/tessdata`

**.zprofile**

```
# TESSDATA
export TESSDATA_PREFIX=/Users/Enoxs/DevTools/tessdata
```

<br>

### Linux

**.bash_profile**
```
# TESSDATA
export TESSDATA_PREFIX=/Users/enoxs/DevTools/tessdata
```

語系參數調整
------
```py
from PIL import Image
import pytesseract
img_name = './002.zh-cht.png'
img = Image.open(img_name)
text = pytesseract.image_to_string(img, lang='chi_tra+eng')
print(text)
```

+ `img_name = './002.zh-cht.png'` : 調整載入的圖片 `繁體中文`
+ `text = pytesseract.image_to_string(img, lang='chi_tra+eng')` : 圖片轉文字，使用繁體中文與英文
  + 英文 : `eng`
  + 繁體中文 : `chi_tra`
  + 簡體中文 : `chi_sim`

<br>

平時用的程式碼
------
`無 Code Review ，Free Style`

    執行 ocr.py 
    將 image 資料夾內的圖片轉換成文字，
    並且依據原始檔案名稱，保存在 text 資料夾中

```py
from PIL import Image
import pytesseract
import os
from os import listdir
from os.path import isfile, join


def ocrText(fileName):
    img = Image.open(fileName)
    # text = pytesseract.image_to_string(img, lang='eng')
    # text = pytesseract.image_to_string(img, lang='eng+chi_tra')
    text = pytesseract.image_to_string(img, lang='eng+chi_tra+chi_sim')
    return text


def replaceText(str):
    str = str.replace(",", "，")
    text = str.replace(" ", "")
    return text


def save(fileName, text):
    print("text.length => ", len(text))
    with open(fileName, 'w', encoding='UTF-8') as f:
        f.write(text)
        f.close


def main():
    path = '.' + os.sep + 'image'
    lstFile = [f for f in listdir(path) if isfile(join(path, f))]

    for f in lstFile:
        if '.png' in f:
            idx = f.find('.png')
            out_name = ""
            for i in range(idx):
                out_name += f[i]
            print(out_name)
            text = ocrText('.' + os.sep + 'image' + os.sep + '{}'.format(f))
            # text = replaceText(text)
            path = '.' + os.sep + 'text' + os.sep + out_name + '.txt'
            save(path, text)


if __name__ == "__main__":
    main()
```
+ `ocrText()` : 將目標圖片轉換成文字
+ `replaceText()` : 依據需求替換指定文字
+ `save()` : 將文字保存到指定路徑
+ `main()` : 整體流程
  1. 取得 image 資料夾下所有的檔案名
  2. 使用 for in 逐筆轉換文字與處理檔名
  3. 轉換完成後，將文字保存到 text 資料夾


Reference
------
### GitHub - Tesseract
<https://github.com/UB-Mannheim/tesseract/wiki>

### iT 邦幫忙 - 聽過 OCR 嗎? 實作看看吧 -- pytesseract
<https://ithelp.ithome.com.tw/articles/10227263>

### [Python] 5.光學字元辨識(OCR)，圖片辨識文字
<https://www.dotblogs.com.tw/RYNote/2021/01/14/105447>

### Pytesseract 辨識圖片中的文字
<https://lufor129.medium.com/pytesseract-%E8%BE%A8%E8%AD%98%E5%9C%96%E7%89%87%E4%B8%AD%E7%9A%84%E6%96%87%E5%AD%97-b1024f678fac>

### mac 安裝tesseract、pytesseract， 實現圖片裏文字的識別
<https://www.twblogs.net/a/5c016054bd9eee7aec4ebd26>

[OCR]:https://zh.wikipedia.org/wiki/%E5%85%89%E5%AD%A6%E5%AD%97%E7%AC%A6%E8%AF%86%E5%88%AB



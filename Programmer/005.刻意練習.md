如何成為高手 ?
======
`刻意練習 x 建構心智表徵的七大要素`

![cover](assets/005/cover.png)

[`Youtube : 刻意練習 : 建構心智表徵的七大要素 | 職場 x 技能 | 如何成為高手 ? 精通幾乎任何事物的訣竅【Gamma Ray 軟體工作室】`](https://youtu.be/7GR2CgH7RxA)


目錄
------
+ 前言 : 為了技能精通
+ 理論 : 心智表徵
+ 作法 : 刻意練習
+ 分享 : 個人理解
+ 入門到精通

<br>

前言 : 為了技能精通
------
過去我曾在 [軟技能 : 十步學習法] 的影片，說明過如何快速學習一項新技能的方法。

其中的**關鍵點 就是先學習 80/20 法則中，最重要的 20 %**

![001](assets/005/001.80-20.jpeg)

不過達成的目標僅僅是為了**「入門」**，距離要成為獨當一面的「專業人士」(軟體工程師)，還有剩下的 80 %。

**這次的分享的刻意練習，就是為了加速這一過程(少走一些彎路)，達成技能的 「精通」。**

<br>

這次的內容會先從書中的理論基礎 - 心智表徵開始講起。

**高手們就是因為具備了「心智表徵」，所以才能夠，更快的理解資訊更快的解決問題。**

接著會再介紹建構心智表徵的幾個具體作法，最後則是分享一些我對刻意練習的個人理解以及實際應用。

![002](assets/005/002.content.jpeg)


[軟技能 : 十步學習法]:https://youtu.be/Ya7XE47gPBo

<br>


理論 : 心智表徵
------
### 什麼是心智表徵 ?

**書中定義:**

```
心智表徵是一種心智結構，對應著某物品、某概念、一系列資訊，或者是任何出現於腦中的具體或抽象事物。
```

可以這樣來理解，心智表徵描述的實際上就是人類大腦中儲存的各種數據。

![003](assets/005/003.mind-data.jpeg)

這些數據可能各自獨立也有可能互相關聯，而心智表徵代指的就是**特定領域中，互相關聯的各種知識**。

<br>

### 強化心智表徵

**除了意味著增加更多的數據資料，也同時代表要加強資料之間彼此的連結。**

![004](assets/005/004.increase.jpeg)

<br>

以資料庫來比喻的話，就是**更多的資料、更細的分類、更好的索引**。

![005](assets/005/005.database.jpeg)

<br>

達成的效果就是除了最基本的**新增、讀取、修改、刪除**變得更加快速以外，還有多餘的精力可以執行**複合式的動作**。

例如 : 兩組資料合併查詢 (Join) 或者 先查詢後新增，這些能**大幅縮減時間的進階動作**。

![006](assets/005/006.effect.jpeg)

<br>

### 大部分的人都具備「心智表徵」
理解「心智表徵」代表的涵意，就可以知道大部分的人們，基本上都具備了心智表徵的能力。

**只是差異在資料數量的多寡，以及連結範圍的深淺。**

<br>

例如 : 

```
世界上有一個國家叫作「澳洲」，裡面有一個大城市叫作「雪梨」。
大部分的人們可能不知道的事，澳洲的首都不是「雪梨」而是「坎培拉」。
```

一般來說知道這些訊息的，可能大部分都不是專家，有可能只是住在附近的人們或者是時常旅行的部落客。

![007](assets/005/007.Australia.jpeg)

**他們與我們的差異，僅是在建立了這部分的心智表徵**

<br>

### 強化特定領域的心智表徵

原本看起來隨機或混亂的事務，就會知道他的模式理解他的含義。

![008](assets/005/008.domain.jpeg)

如此也就能夠**更快速的回應問題，找到最佳的解決方法。**

<br>

### 正向反饋
心智表徵的好處，除了明顯的技術能力提升以外，兩者之間還會形成一個正循環 :

![009](assets/005/009.iteration.jpeg)

因為高超的技能代表成熟的心智表徵，而成熟的心智表徵又有助於在該領域組織相關知識，制定合理的學習計畫。

**兩者相互迭代的效果，最終就會讓有意識強化心智表徵的練習者，看起來更像是專家一樣。**

<br>


作法 : 刻意練習
------
### 如何強化心智表徵 ?

統整了書中各章節強調的練習重點，總共七大項要素 :

1. 具體目標
2. 跨出舒適圈
3. 專注投入
4. 維持動機
5. 意見回饋
6. 模仿專家
7. 嘗試新方法

<br>

### 具體目標

**書中原文:**

```
刻意練習必須有定義清楚明確的目標，且往往涉及改進想要達到表現的某個面向，而不能只設定模糊的整體改善目標。
```

這種先關注明確的小目標，然後再逐步疊加成大目標的方法。

在其他的學習法，例如: 十步學習法 - 第三步 定義目標，也有提及。

![010](assets/005/010.ten-step-learning-method.jpeg)

<br>

**目標練習法的重點**
1. 清晰優於模糊
2. 小目標優於大目標

```
因為一個清晰又容易達成的小目標，以完成的成功率而言，機率肯定是大於模糊且遙遙無期的大目標。
```

![011](assets/005/011.success-rate.jpeg)

這邊要做的事情，就是先幫你克服練習的最大靜摩擦力。`(只有開始練習了，後續才會知道還有什麼東西需要練習)`

<br>

### 跨出舒適圈

**書中原文**

```
刻意練習只有在跨出舒適圈後才能奏效，需要學生不斷嘗試去突破現階段的技能水準。
(這意味著幾乎得傾盡全力，所以不會太有樂趣。)
```

從這一段話可以知道，真正會帶來進步效果的「刻意練習」，過程並不會太令人開心。

你的身體或者是腦神經細胞，正是因為感受到這種**壓迫**，所以才會發生**變化**。

![012-1](assets/005/012-1.zone.jpeg)

目的正是希望以提升性能的方式，擴大舒適圈讓身體不再難受。

    在練習技能時可以以是否感受到舒適或者困難，作為判斷訓練時強度的依據。

<br>

**注意練習強度**

雖然說要誇出舒適圈，才能夠達到練習的效果，但又不能夠離舒適圈太遠。

![012-2](assets/005/012-2.zone.jpeg)

**如果逼迫自己到恐慌圈的部分，反而可能造成傷害導致技能退步。**

<br>

### 專注投入

**書中原文 :**
```
刻意練習是「刻意」進行的，也就是必須全神貫注，有意識的行動。
```

這邊以舒適圈的角度來看，不夠專注、漫不經心的練習是否待在舒適圈不好說，但肯定是沒有待在學習圈。

![013](assets/005/013.focus-zone.jpeg)

**因為你的身體或者腦神經並沒有感到壓力，這樣就更不要說催化他們增強性能。**

	當你在練習時練到有些分心或者覺得有些輕鬆好玩時，
	不如休息一下，因為此時練習花費的時間可能都是無效的。

<br>

**維持專注力簡單可行的方法**

```
為自己預留一個「單獨練習」的時間，排除像手機或者是其他可能干擾的因素，並且初期不需要長，
以最多可以維持多長的專注力為基礎，十分鐘、二十分鐘、半小時，都是可以接受的。
```

<br>

### 維持動機
由「第二項 - 跨出舒適圈」與「第三項 - 專注投入」，

可以知道刻意練習時，必須全神貫注待在不那麼令人舒服的學習區，耗費的心神是極大的。

<br>

**如何持續的堅持下去 ?**

書中認為與其關注「意志力」，這種科學難以證明統一通用的事務，不如討論「動機」會更有幫助。

![014-1](assets/005/014-1.motivation.jpeg)

這部分，雖然說非常的因人而異，而且在不同的時間點、不同的情境下，強弱的程度也會也所不同。

但總體來說要做的事情就兩件 :
1. 減少動機的消失
2. 增加動機的來源

<br>

**減少動機的消失**

練習時，保持**充足睡眠、均衡飲食、身體健康**，都可以讓你有更多的專注力可以使用。

![014-2](assets/005/014-2.motivation.jpeg)

並且再搭配剛才「維持專注力」提到的，一個獨自的練習時間、排除一切的干擾，

然後時間控制在人類**專注力極限的一個小時左右**。

    配合適當的休息、保持良好狀態，都是維持動機的良好方法。

<br>

**增加動機的來源**

其中三種我認為比較重要的，可以劃分成短、中、長三個階段:
1. 短期 : 先產生練習的動機
2. 中期 : 持續產生練習的動機
3. 長期 : 相信獲得成果的動機

<br>

短期 : 先產生練習的動機

```
最好也是最強大的，是外部動機之一的「社會動機」(單純直接、他人認可的動機)。

具體做法就是去參與共同練習的團體，讓自己的周遭，時常圍繞著會給你鼓勵、支持和挑戰的人。
```

![015-1](assets/005/015-1.social-motivation.jpeg)

<br>

中期 : 持續產生練習的動機

```
搭配 「第一項 - 具體目標」
當練習出現一定成果後，完成的小目標帶來明顯可見的技能提升。

這種提升帶來的滿足感，也能夠讓你成為動機的一部分。
```

![015-2](assets/005/015-2.target.jpeg)

<br>

長期 : 相信獲得成果的動機

```
當已經投入了足夠多的時間並且已經看到明顯的反饋後，相信自己能夠看到最終結果的動機就會逐步成形。

這種動機除了非常強大以外，還能夠延續到其他的技能練習上。(因為既然之前都能夠有效練習，那麼這次挑戰也肯定能成功!)
```

![015-3](assets/005/015-3.believe.jpeg)

<br>

### 意見回饋 & 模仿專家

「第五項 - 意見回饋」與「第六項 - 模仿專家」，可以合併一起說明。

**書中原文:**
```
意見回饋 : 
刻意練習包含意見回饋，並根據該回饋調整努力方向。

模仿專家 : 
刻意練習培養的技能，已經有其他人知道該怎麼做，也已建立成效頗佳的訓練技巧。
```

這兩種都要依賴熟悉相關知識的老師或者是教練才能夠達成。

之所以將這兩種排序的比較後面，是因為有時候並沒有那麼多的資源，這兩個部份就只能靠自己。

<br>

**意見回饋 :**
```
該部分比較沒問題，因為不管有沒有其他人在旁邊監督指導，最終想要達到的目標 都是讓學生能夠自我監督、察覺錯誤、因應調整。

如此才能夠在獨自練習時，有效的建立正確的心智表徵。
```

![016](assets/005/016.feedback.jpeg)

<br>

**模仿專家 :**

```
原意是先獲取該領域最頂尖的相關知識，然後再以此為基礎向上發展。(練習的起點已經贏過他人，成為專家的路途就縮短許多)

只不過可能發生的情況，就如同先前一樣，沒有那麼多的資源，所以替代方案就是去尋找一個頂尖的專家進行模仿。
雖然可能沒有直接指導來的高效，但多少也能達到一定的效果。
```

![017](assets/005/017.expert.jpeg)

<br>

### 嘗試新方法
`「第六項 - 模仿專家」的延伸`

如果你有足夠的條件可以以頂尖的前輩為師，你應該已經弄清楚那些頂尖專家，都做了些什麼事情才能夠如此成功。

	某種意義上來說，練習的成效如果不錯的話，你應該也算是該領域的專家。

<br>

**如何繼續向上突破 ?**

靠「試誤法」來自行摸索。

```
簡單來說就是換一種方法來嘗試練習，然後根據「第五項 - 意見回饋」，判斷練習是否俱有成效。

這種方法也是在沒有老師或者教練協助指導時，另外一種可以有效突破現階段能力的好方法。
```

![018](assets/005/018.try-and-error.jpeg)

<br>

分享 : 個人理解
------
### 心智表徵的部分

我認為他想要達成的目的，就是在執行任務時，能夠**協調一致的行動**

```
不管是身體與大腦的協調，還是大腦之中各個相關知識的協調;
在做該領域的任何的事務時，都可以完全調動發揮應有的能力。
```

起碼不會明明身體力量足夠，但卻因為姿勢不對或者是發力方法錯誤，**導致你應該有 80 分的能力，結果卻得到 60 分的情況發生。**

<br>

### 七大要素 : 俱體目標
原意是清楚的小目標是好的，不過我觀察到書中，那些可以真正套用刻意練習的領域。

例如 : **音樂、運動、下棋**，他們之中都有相關比賽，是可以讓練習這個領域的人們分出輸贏。

![019](assets/005/019.contest.jpeg)



<br>

所以另外一層的理解 : 以大目標而言，如果明確的定義說我要在**比賽中取得最終的勝利**(擊敗所有對手)

後面二到四項要素就會立刻迎刃而解:

![020-1](assets/005/020-1.win.jpeg)

```
因為如果是為了贏想要擊敗對手，跨出舒適圈忍受小小的不舒服，就不會是什麼大不了的事情。
而且也因為知道目標就是為了贏，練習時就肯定會專注投入。
```

<br>

這種想要獲得勝利的動機，實際上就是人類最原始的本能 : **「戰鬥」 或者 「逃跑」**

![020-2](assets/005/020-2.fight-or-flee.jpeg)

<br>

而且刻意練習建立的心智表徵，除了能夠幫助戰鬥取得優勢以外，對於判斷是否要逃跑也是重大的助益。

![020-3](assets/005/020-3.run.jpeg)

<br>

### 七大要素 : 意見回饋

我認為非常的像是應用程式的使用者體驗`(UX , User Experience)`

例如 : 點擊按鈕時元件點擊的效果或者是下載檔案時顯示狀態的進度條

![021](assets/005/021.ux.jpeg)

```
如果沒有這些可以知道自己做的動作是否有效的反饋，就會覺得這個程式卡卡的好像當機沒有反應。

這時候就會煩躁，不想要再用下去。
```

**對比於練習的話，就會覺得練習沒有效果，不想要再繼續下去。**

<br>

### 獨自練習

我認為在沒有老師或教練的情況下要獲得意見回饋，最好的方法就是「紀錄」。

**紀錄主要有三個時間點 :**

![022](assets/005/022.record.jpeg)

**第一個時間點**

在做這件事情之前，紀錄「預期」應該會得到的結果。

**第二個時間點**

做了之後，記錄「實際上」得到的結果。

**第三個時間點**

找一段時間，靜下心分析這兩段時間到底做了什麼事情達到預期效果，而又做了哪些事情導致沒有達到目的。

	最後，將這些東西統整後擬定下一次的行動方針，然後在從第一點重複循環。

<br>

**長久下來，觀察這些記錄的資料，你就可以確切的掌握心智表徵已經發展成什麼樣子。**

<br>

### 七大要素 : 模仿專家

不得不說，程式設計的領域是非常幸運的，因為是跟電腦相關，所以網路上就會有非常多的資料。

不過就如同尋找頂尖專家一樣，最好是根據一些客觀具體的技能指標來判斷。

```
以程式設計的領域來說，常做的事情是創建新的事物。
所以「是否有條理」或者「是否有邏輯」，可以用來判定。
```

<br>

當然最直觀的，還是作者創作出來的**作品**或者是撰寫出來的**程式碼** 

![023-1](assets/005/023-1,bootstrap-se.jpeg)
+ [`YouTube : 網頁設計入門 - 如何使用 Bootstrap 與 Github Pages 製作 個人網站 ?`](https://youtu.be/ctr6eo9dL-A)

<br>

![023-2](assets/005/023-2.mark-account.jpeg)
+ [`YouTube : 馬克記帳 - 以 Markdown 為基礎的記帳 App`](https://youtu.be/JFDq6AkifJs)

<br>

	這些都是可以更直接的了解，找到的知識是否具備足夠的模仿的價值。

<br>


入門到精通
------

以上，就是關於我對刻意練習的理解。

我運用刻意練習有一些技能學習的還不錯，例如: 程式設計或者是程式設計的工具

![024](assets/005/024.program.jpeg)
+ [`YouTube : 軟體的本質又是什麼 ?`](https://youtu.be/njAgG9e1Qco)
+ [`YouTube : 軟體如何設計 ?`](https://youtu.be/mducM_tUUEY)
+ [`YouTube : Vim 編輯器 入門指南`](https://youtu.be/Yk4s-WLjxug)
+ [`YouTube : IntelliJ 入門指南`](https://youtu.be/FkL17L_gokc)


而有一些技能還需要加強與鍛鍊，例如: 英文能力或者是錄製影片的聲音語調。

    製作這期影片的目的之一，就是為了強化這些技能，所以翻找出以前看過的書籍彙整分享。

<br>

我認為「刻意練習」可以搭配之前，分享過的「十步學習法」一起使用。

兩者高低搭配 : 
+ 「十步學習法」運用在快速學習新技能，也就是 「入門」 的部分。
+ 「刻意練習」 運用在強化技能，也就是 「精通」 的部分。

**運用在職場，不管是廣度還是深度的技能提升，都能夠有一套成熟與高效的學習方案。**

<br>


參考資料
------
+ 書籍 : 刻意練習 - 原創者全面解析，比天賦更關鍵的學習法


